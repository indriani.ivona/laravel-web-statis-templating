@extends('layout.master')
@section('title')
Halaman Pendaftaran
@endsection
@section('content')
        <h2>Sign Up Form</h2>
        <form action="/welcome" method="POST">
            @csrf
            <label>Name :</label><br>
            <input type="text" name="nama_lengkap"><br><br>
            <label>Gender</label><br>
            <input type="radio" name="gen">Male <br>
            <input type="radio" name="gen">Female <br><br>
            <label>Nationality</label><br>
            <select name="nat">
                <option>Indonesia</option>
                <option>Malaysia</option>
                <option>Singapore</option>
                <option>Philippine</option>
                <option>Thailand</option>
            </select>
            <br><br><label>Languange Spoken</label><br>
                <input type="checkbox" name="lang">Bahasa Indonesia <br>
                <input type="checkbox" name="lang">English <br>
                <input type="checkbox" name="lang">Other <br><br>
            <label>Bio</label><br>
            <textarea name="bio" cols="40" rows="10"></textarea><br><br>
            <input type="submit" value="Sign Up">
        </form>
        <a href="/">kembali</a>
@endsection