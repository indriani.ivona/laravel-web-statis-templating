<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('page.register');
    }

    public function welcome(Request $request){
        $nama = $request['nama_lengkap'];
        $nat = $request['nat'];

        return view('page.welcome', compact("nama","nat"));
    }
}
