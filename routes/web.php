 <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@bio');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/data-tables', 'IndexController@table');

//CRUD Cast
Route::get('/cast/create', 'CastController@create'); //form membuat data pemain baru
Route::post('/cast', 'CastController@store'); //menyimpan data baru ke DB
Route::get('/cast', 'CastController@index'); //menampilkan data
Route::get('/cast/{cast_id}', 'CastController@show'); //menampilkan detail cast
Route::get('cast/{cast_id}/edit', 'CastController@edit'); //menyimpan perubahan data untuk id tertentu
Route::put('cast/{cast_id}/', 'CastController@update'); //menyimpan perubahan data dari edit
Route::delete('cast/{cast_id}', 'CastController@destroy'); //menghapus data berdasarkan id tertentu